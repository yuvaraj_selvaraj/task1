<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Survey HTML Form </title>

    <!-- load CSS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600">
    <!-- Google web font "Open Sans" -->
    <link rel="stylesheet" href="css/materialize.min.css">
    <!-- https://materializecss.com -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- https://getbootstrap.com/ -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- https://fontawesome.com/ -->
    <link rel="stylesheet" href="css/templatemo-style.css">

</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-7">
            <i class="far fa-comments fa-5x tm-site-icon"></i>
            <h1 class="tm-site-name">Survey Form</h1>
        </div>
        <div class="col-sm-12 col-md-5 mt-md-0 mt-5">
            <div class="tm-site-header-right-col">
                <a href="#" class="btn btn-primary">Contact Us</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="tm-parallax"></div>
        </div>
    </div>
    <div class="row">
        <div class="tm-intro">
            <div class="col-sm-12 col-md-8 mb-md-0 mb-4 tm-intro-left">
                <p class="mb-0">Social media are interactive computer-mediated technologies that facilitate the creation or sharing of information, ideas, career interests, and other forms of expression via virtual communities and networks. Social media is any digital tool that allows users to quickly create and share content with the public. Thank you.</p>
            </div>
            <div class="col-sm-12 col-md-4">
                <em>
                    <p class="tm-highlight">&ldquo;Your Opinion Matters&rdquo;</p>
                </em>
            </div>
        </div>
        <div class="col-12">
            <hr>
        </div>
    </div>

    <form action="response.php" method="post" enctype="multipart/form-data">
        <div class="row">
            <div class="col-12">
                <h2 class="tm-question-header">Question 1</h2>
                <p>Considering your complete experience with social media websites (Facebook, Twitter, Instagram etc.), how likely are you to recommend it to your family and friends?</p>
                <div class="tm-q-choice-container">
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q1" type="radio" value="1" />
                            <span>A. 1</span>
                        </div>
                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q1" type="radio" value="2" />
                            <span>B.2</span>
                        </div>

                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q1" type="radio" value="3" />
                            <span>C.3</span>
                        </div>

                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q1" type="radio" value="4" />
                            <span>D. 4</span>
                        </div>

                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q1" type="radio" value="5" />
                            <span>E. 5</span>
                        </div>

                    </label>
                </div>
            </div> <!-- col-12 -->
            <div class="col-12">
                <hr>
            </div>

            <div class="col-12">
                <h2 class="tm-question-header">Question 2</h2>
                <p>What is your go-to device to access your social media feed?</p>
                <div class="tm-q-choice-container">
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q2" type="radio" value="Mobile" />
                            <span>A.Mobile</span>
                        </div>
                        <img src="img/mobile.jfif" alt="Image">
                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q2" type="radio" value="Tablet" />
                            <span>B. Tablet</span>
                        </div>
                        <img src="img/tablet.jfif" alt="Image">
                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q2" type="radio" value="Laptop" />
                            <span>C. Laptop</span>
                        </div>
                        <img src="img/lap.jfif" alt="Image">
                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q2" type="radio" value="Desktop" />
                            <span>D.Desktop</span>
                        </div>
                        <img src="img/desktop.jfif" alt="Image">
                    </label>

                </div>
            </div> <!-- col-12 -->
            <div class="col-12">
                <hr>
            </div>

            <div class="col-12">
                <h2 class="tm-question-header">Question 3</h2>
                <p>How often do you check-in to your social media accounts in any given week?</p>
                <div class="tm-q-choice-container">
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q3" type="radio" value="Daily" />
                            <span>A. Daily</span>
                        </div>
                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q3" type="radio" value="Every One Day" />
                            <span>B.Every other day</span>
                        </div>

                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q3" type="radio" value="Every Two Days" />
                            <span>C.Every two days</span>
                        </div>

                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q3" type="radio" value="Once a Week" />
                            <span>D.Once a week</span>
                        </div>

                    </label>
                    <label class="tm-q-choice">
                        <div class="mb-3">
                            <input class="tm-radio-group-1 with-gap" name="q3" type="radio" value="Every hour" />
                            <span>E.Every hour</span>
                        </div>

                    </label>
                </div>
            </div> <!-- col-12 -->
            <div class="col-12">
                <hr>
            </div>
            <div class="col-12">
                <h2 class="tm-question-header tm-question-header-mt">Question 4</h2>
                <p>In a week, which of the following social media websites do you visit most frequently?</p>
                <div class="tm-q-choice-container">
                    <label class="tm-q-choice tm-q-choice-2-col">
                        <div class="mb-3">
                            <input type="checkbox" name="q4[]" class="filled-in tm-checkbox" value="Linkedin" />
                            <span>Linkedin</span>
                        </div>
                        <img src="img/linkedin.jfif" alt="Image" class="img-fluid">
                    </label>
                    <label class="tm-q-choice tm-q-choice-2-col">
                        <div class="mb-3">
                            <input type="checkbox" name="q4[]" class="filled-in tm-checkbox" value="FaceBook" />
                            <span>FaceBook</span>
                        </div>
                        <img src="img/fb.jfif" alt="Image" class="img-fluid">
                    </label>
                    <label class="tm-q-choice tm-q-choice-2-col">
                        <div class="mb-3">
                            <input type="checkbox" name="q4[]" class="filled-in tm-checkbox" value="Twitter" />
                            <span>Twitter</span>
                        </div>
                        <img src="img/twitter.jfif" alt="Image" class="img-fluid">
                    </label>
                    <label class="tm-q-choice tm-q-choice-2-col">
                        <div class="mb-3">
                            <input type="checkbox" name="q4[]" class="filled-in tm-checkbox" value="Instagram" />
                            <span>Instagram</span>
                        </div>
                        <img src="img/insta.jfif" alt="Image" class="img-fluid">
                    </label>
                </div>
            </div> <!-- col-12 -->
            <div class="col-12">
                <hr>
            </div>
            <div class="col-12">
                <h2 class="tm-question-header tm-question-header-mt">Question 5</h2>
                <p class="mb-0">Please rate the following social media websites on a scale from 0-10</p>
                <div class="tm-q3-choice-container">
                    <label class="tm-q3-choice tm-q-choice-2-col">
                        <img src="img/fb.jfif" alt="Image" class="img-fluid">
                        <div class="mb-3">
                            <input type="range" name="q5[]" class="filled-in tm-checkbox"  min="0" max="10" />
                            <span>Facebook</span>
                        </div>
                    </label>
                    <label class="tm-q3-choice tm-q-choice-2-col">
                        <img src="img/insta.jfif" alt="Image" class="img-fluid">
                        <div class="mb-3">
                            <input type="range" name="q5[]" class="filled-in tm-checkbox"  min="0" max="10"/>
                            <span>Instagram</span>
                        </div>
                    </label>
                    <label class="tm-q3-choice tm-q-choice-2-col">
                        <img src="img/linkedin.jfif" alt="Image" class="img-fluid">
                        <div class="mb-3">
                            <input type="range" name="q5[]" class="filled-in tm-checkbox" value="q5_a3" min="0" max="10"/>
                            <span>linkedin</span>
                        </div>
                    </label>
                    <label class="tm-q3-choice tm-q-choice-2-col">
                        <img src="img/twitter.jfif" alt="Image" class="img-fluid">
                        <div class="mb-3">
                            <input type="range" name="q5[]" class="filled-in tm-checkbox" value="q5_a4" min="0" max="10" />
                            <span>twitter</span>
                        </div>
                    </label>

                </div> <!-- choices container -->
            </div> <!-- col-12 -->

            <div class="col-12 tm-respondent-info">
                <hr>
                <h2 class="tm-question-header tm-question-header-mt">Please tell a little bit about yourself</h2>
                <div class="row">
                    <div class="col-md-6 col-lg-4">
                        <div class="form-group">
                            <label for="inputName">Your Name</label>
                            <input type="text" class="form-control browser-default" id="inputName" name="name"
                                   aria-describedby="nameHelp">
                        </div>
                        <div class="form-group">
                            <label >Your Email</label>
                            <input type="email" class="form-control browser-default" id="inputEmail1" name="email"
                                   aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label >Your DOB</label>
                            <input type="date" class="form-control browser-default" id="DOB" name="dob"
                                   aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label >Phone number</label>
                            <input type="text" class="form-control browser-default" id="pno" name="pno"
                                   aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label>Age</label>
                            <input type="number" class="form-control browser-default" id="age" name="age"
                                   aria-describedby="emailHelp">
                        </div>
                    </div>

                    <div class="col-md-6 col-lg-4">
                        <div class="tm-choice-mt">
                            <label class="tm-q-choice">
                                <input type="radio" name="gender" class="with-gap" value="male" />
                                <span>Male</span>
                            </label>
                            <label class="tm-q-choice tm-choice-ml">
                                <input type="radio" name="gender" class="with-gap" value="female" />
                                <span>Female</span>
                            </label>
                        </div>
                        <div class="tm-choice-mt">
                        <div class="form-group">
                            <label>upload photo</label>
                            <input type="file" name="fileToUpload" id="fileToUpload">
                        </div>
                        </div>
                        <div class="input-field col tm-dropdown-container" disabled="disabled">
                            <select class="" name="occupation" id="occupation">
                                <option value="select">Your Occupation</option>
                                <option value="Cheif Executive">Chief Executive</option>
                                <option value="Software Architect">Software Architect</option>
                                <option value="Project Manager">Project Manager</option>
                                <option value="Web Developer">Web Developer</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-12 col-lg-4">
                        <label for="message" class="mb-3">Additional Opinions</label>
                        <textarea class="p-3" name="message" id="message" cols="30" rows="3"></textarea>
                    </div>
                    <div class="col-md-6 col-lg-4">
                        <div class="form-group">
                            <label>Favorite timepass</label>
                            <select class="" name="favorite[]" id="favorite" multiple placeholder="select the favorite">
                                <option value="Chess">chess</option>
                                <option value="SocialMedia">Social media</option>
                                <option value="Carrom">carrom</option>
                                <option value="Cricket">cricket</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center tm-submit-container">
                <button type="submit" href="#" class="btn btn-primary tm-btn-submit">Submit</button>
            </div>
            <div class="col-12">
                <hr>
            </div>
        </div> <!-- row -->
    </form>
    <div class="row">
        <div class="col-12">
            <footer>
                <p class="text-center mb-3 tm-footer-text">Copyright &copy; 2020 Social media Survey - HTML Form by Yuvaraj Selvaraj</p>
            </footer>
        </div>
    </div>
</div>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/materialize.min.js"></script>
<script>
    var background_image_parallax = function ($object, multiplier) {
        multiplier = typeof multiplier !== 'undefined' ? multiplier : 0.5;
        multiplier = 1 - multiplier;
        var $doc = $(document);
        $object.css({ "background-attatchment": "fixed" });
        $(window).scroll(function () {
            var from_top = $doc.scrollTop(),
                bg_css = 'center ' + (multiplier * from_top - 200) + 'px';
            $object.css({ "background-position": bg_css });
        });
    };
    function detectIE() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }
        return false;
    }

    $(document).ready(function () {

        // Detect IE
        if (detectIE()) {
            alert('Please use the latest version of Chrome, Firefox, or Edge for best browsing experience.');
        }

        $('select').formSelect();
        // Parallax image background
        background_image_parallax($(".tm-parallax"), 0.40);

        // Darken image when its radio button is selected
        $(".tm-radio-group-1").click(function () {
            $('.tm-radio-group-1').parent().siblings("img").removeClass("darken");
            $(this).parent().siblings("img").addClass("darken");
        });

        $(".tm-radio-group-2").click(function () {
            $('.tm-radio-group-2').parent().siblings("img").removeClass("darken");
            $(this).parent().siblings("img").addClass("darken");
        });

        $(".tm-checkbox").click(function () {
            $(this).parent().siblings("img").toggleClass("darken");
        })
    });
</script>
</body>

</html>